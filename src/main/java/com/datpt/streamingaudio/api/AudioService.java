package com.datpt.streamingaudio.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.datpt.streamingaudio.business.AudioBusiness;
import com.datpt.streamingaudio.common.CommonService;
import com.datpt.streamingaudio.database.bo.AudioDetailBO;
import com.datpt.streamingaudio.database.entity.Audio;
import com.datpt.streamingaudio.req.AddNewAudioReq;
import com.datpt.streamingaudio.req.CommentReq;
import com.datpt.streamingaudio.req.DeleteAudioReq;
import com.datpt.streamingaudio.req.GetDetailAudioReq;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author DatPt
 * @date Dec 28, 2020
 * 
 */

@RestController
@RequestMapping("/audio_service")
@Api(value = "audio service", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class AudioService extends CommonService {

	@Autowired
	private AudioBusiness business;
	
	@RequestMapping(value = "/add_new_audio", method = RequestMethod.POST)
	@ApiOperation(value = "Add new audio", response = String.class)
	public String addNewAudio(
			@RequestBody AddNewAudioReq request) throws Exception{
				
		return business.addNewAudio(
				request.getUsername(),
				request.getPassword(), 
				request.getAudioName(), 
				request.getAudioType(), 
				request.getAudioSrc(), 
				request.getAudioImageSrc(),
				request.getAudioAuthor());
	}
	
	@RequestMapping(value = "/delete_audio", method = RequestMethod.POST)
	@ApiOperation(value = "delete audio", response = String.class)
	public String deleteAudio(
			@RequestBody DeleteAudioReq request) throws Exception{
				
		return business.deleteAudio(
				request.getUsername(), 
				request.getPassword(), 
				request.getAudioId());
	}
	
	@RequestMapping(value = "/get_detail_audio", method = RequestMethod.POST)
	@ApiOperation(value = "Get DetailAudio", response = AudioDetailBO.class)
	public AudioDetailBO getDetailAudio(
			@RequestBody GetDetailAudioReq request) throws Exception{
				
		return business.getDetailAudio(request.getAudioId());
	}
	
	@RequestMapping(value = "/get_list_audio", method = RequestMethod.GET)
	@ApiOperation(value = "Get DetailAudio", response = Audio[].class)
	public List<Audio> getListAudio() throws Exception{
				
		return business.getListAudio();
	}
	
	@RequestMapping(value = "/up_view", method = RequestMethod.POST)
	@ApiOperation(value = "up view", response = AudioDetailBO.class)
	public String upView(
			@RequestBody GetDetailAudioReq request) throws Exception{			
		return business.upView(request.getAudioId());
	}
	
	@RequestMapping(value = "/comment", method = RequestMethod.POST)
	@ApiOperation(value = "comment", response = String.class)
	public String comment(
			@RequestBody CommentReq request) throws Exception{			
		return business.comment(
				request.getUsername(), 
				request.getPassword(), 
				request.getAudioId(), 
				request.getContent());
	}
	
		
}

