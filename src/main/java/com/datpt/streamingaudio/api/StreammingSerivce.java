package com.datpt.streamingaudio.api;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.datpt.streamingaudio.common.CommonService;
import com.datpt.streamingaudio.database.entity.Audio;
import com.datpt.streamingaudio.database.repo.AudioRepo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author DatPt
 * @date Jan 22, 2021
 * 
 */

@RestController
@RequestMapping("/streaming_service")
@Api(value = "streaming service", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class StreammingSerivce extends CommonService{
	
	@Autowired
	private AudioRepo audioRepo;
	  
	@RequestMapping(value = "/stream_audio", method = RequestMethod.GET)
	@ApiOperation(value = "Stream Audio", response = String.class)
	public ResponseEntity<byte[]> streamAudio(
			@RequestParam("audioId") Long audioId) throws IOException{
		
		Audio audio = audioRepo.findOneByAudioId(audioId);
		
		File file = new File(audio.getAudioSrc());
 
		byte[] fileContent = Files.readAllBytes(file.toPath());
		return ResponseEntity.status(HttpStatus.OK)
			 .header("Content-Type", "audio/mp3")
			 .header("Accept-Ranges", "bytes")
			 .header("Content-Length", String.valueOf(file.length()))	
			 .body(fileContent);	  
	}

}
