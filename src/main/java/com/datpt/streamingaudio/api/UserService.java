package com.datpt.streamingaudio.api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.datpt.streamingaudio.business.UserBusiness;
import com.datpt.streamingaudio.common.CommonService;
import com.datpt.streamingaudio.req.ChangePasswordReq;
import com.datpt.streamingaudio.req.CreateUserReq;
import com.datpt.streamingaudio.req.LoginReq;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author DatPt
 * @date Dec 28, 2020
 * 
 */

@RestController
@RequestMapping("/user_service")
@Api(value = "user service", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class UserService extends CommonService {

	@Autowired
	private UserBusiness business;
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@ApiOperation(value = "Login to streaming server", response = String.class)
	public String login(
			@RequestBody LoginReq request) throws Exception{
				
		return business.login(request.getUsername(), request.getPassword());
		
	}
	
	@RequestMapping(value = "/create_user", method = RequestMethod.POST)
	@ApiOperation(value = "create user streaming server", response = String.class)
	public String createUser(
			@RequestBody CreateUserReq request) throws Exception{
				
		return business.createUser(
				request.getEmail(),
				request.getUsername(),
				request.getPassword());
		
	}
	
	@RequestMapping(value = "/change_password", method = RequestMethod.POST)
	@ApiOperation(value = "change password streaming server", response = String.class)
	public String changePassword(
			@RequestBody ChangePasswordReq request) throws Exception{
				
		return business.changePassword(
				request.getUsername(),
				request.getOldPassword(),
				request.getNewPassword());
		
	}
	
		
}

