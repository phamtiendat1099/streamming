package com.datpt.streamingaudio.api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.datpt.streamingaudio.business.FileUploadBusiness;
import com.datpt.streamingaudio.common.CommonService;
import com.datpt.streamingaudio.req.UploadFileReq;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author DatPt
 * @date Dec 28, 2020
 * 
 */

@RestController
@RequestMapping("/file_upload_service")
@Api(value = "File upload service", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class FileUploadService extends CommonService {

	@Autowired
	private FileUploadBusiness business;
	
	@RequestMapping(value = "/upload_file", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	@ApiOperation(value = "Upload File to streaming server", response = String.class)
	public String uploadFile(
			@RequestBody MultipartFile file) throws Exception{		
		return business.uploadFile(file);
	}
	
	
	
		
}

