package com.datpt.streamingaudio.database.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * @author DatPt
 * @date Feb 17, 2021
 * 
 */

@SuppressWarnings("serial")
@Getter
@Setter
@Entity
@Table(name = "AUDIO", schema = "streaming_audio", catalog = "streaming_audio")
public class Audio implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "AUDIO_ID")
	private Long audioId;
	
	@Column(name = "AUDIO_NAME")
	private String audioName;
	
	@Column(name = "AUDIO_TYPE")
	private String audioType;
	
	@Column(name = "AUDIO_SRC")
	private String audioSrc;
	
	@Column(name = "AUDIO_IMAGE_SRC")
	private String audioImageSrc;
	
	@Column(name = "AUDIO_AUTHOR")
	private String audioAuthor;
	
	@Column(name = "VIEW")
	private Long view;
	
	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "CREATED_DATETIME")
	private Date createdDatetime;
	
	@Column(name = "CREATED_USER")
	private String createdUser;
	
	@Column(name = "UPDATED_DATETIME")
	private Date updatedDatetime;
	
	@Column(name = "UPDATED_USER")
	private String updatedUser;
	
}
