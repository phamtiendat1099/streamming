package com.datpt.streamingaudio.database.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * @author DatPt
 * @date Feb 17, 2021
 * 
 */

@SuppressWarnings("serial")
@Getter
@Setter
@Entity
@Table(name = "FILE_UPLOAD", schema = "streaming_audio", catalog = "streaming_audio")
public class FileUpload implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "FILE_ID")
	private Long fileId;
	
	@Column(name = "FILE_TYPE")
	private String fileType;
	
	@Column(name = "FILE_SRC")
	private String fileSrc;
	
	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "CREATED_DATETIME")
	private Date createdDatetime;
	
	@Column(name = "CREATED_USER")
	private String createdUser;
}
 