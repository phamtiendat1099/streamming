package com.datpt.streamingaudio.database.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.datpt.streamingaudio.database.entity.Audio;

/**
 * @author DatPt
 * @date Feb 17, 2021
 * 
 */

public interface AudioRepo extends JpaRepository<Audio, Long>{
	
	public Audio findOneByAudioId(Long audioId);
	
	public List<Audio> findByStatus(String status);

}
