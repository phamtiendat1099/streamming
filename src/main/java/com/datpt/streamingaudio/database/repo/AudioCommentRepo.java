package com.datpt.streamingaudio.database.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.datpt.streamingaudio.database.entity.AudioComment;

/**
 * @author DatPt
 * @date Feb 17, 2021
 * 
 */

public interface AudioCommentRepo extends JpaRepository<AudioComment, Long>{

	public AudioComment findByAudioId(Long audioId);
	
}
