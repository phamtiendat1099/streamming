package com.datpt.streamingaudio.database.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.datpt.streamingaudio.database.entity.User;

/**
 * @author DatPt
 * @date Feb 17, 2021
 * 
 */

public interface UserRepo extends JpaRepository<User, Long>{
	
	public User findOneByUsername(String username);
	
	public User findOneByEmail(String email);
	
	public User findOneByUsernameAndPassword(String username, String password);

}
