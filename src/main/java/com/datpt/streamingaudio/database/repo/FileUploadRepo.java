package com.datpt.streamingaudio.database.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.datpt.streamingaudio.database.entity.FileUpload;

/**
 * @author DatPt
 * @date Feb 17, 2021
 * 
 */

public interface FileUploadRepo extends JpaRepository<FileUpload, Long>{

}
