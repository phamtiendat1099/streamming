package com.datpt.streamingaudio.database.config;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
		basePackages = {"com.datpt.streamingaudio.database.repo"}, 
		entityManagerFactoryRef = "streamingAudioEntityManager", 
		transactionManagerRef = "streamingAudioTransactionManager")
@PropertySource(value = { "classpath:database.properties" })
public class StreamingAudioDatabaseConfig {

	@Autowired
	private Environment environment;
	
	
	@Bean
	@Primary
	public LocalContainerEntityManagerFactoryBean streamingAudioEntityManager() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(streamingAudioDataSource());
		em.setPackagesToScan(new String[] { "com.datpt.streamingaudio.database.entity" });
		em.setPersistenceUnitName("streamingAudio");
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		em.setJpaPropertyMap(getHibernatePropertiesV2());
		return em;
	}
	

	@Bean
	@Primary
	public DataSource streamingAudioDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(environment.getRequiredProperty("db.driverClassName"));
		dataSource.setUrl(environment.getRequiredProperty("db.streaming.audio.url"));
		dataSource.setUsername(environment.getRequiredProperty("db.streaming.audio.username"));
		String password = environment.getRequiredProperty("db.streaming.audio.password");
		dataSource.setPassword(password);
		return dataSource;
	}
	

    @Bean
    @Primary
	public PlatformTransactionManager streamingAudioTransactionManager() {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(streamingAudioEntityManager().getObject());
		return transactionManager;
	}


	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Map<String, Object> getHibernatePropertiesV2() {
		Map properties = new HashMap<String, Object>();
		//properties.put("hibernate.dialect", environment.getRequiredProperty("hibernate.dialect"));
		properties.put("hibernate.show_sql", false);
		properties.put("hibernate.format_sql", false);
		properties.put("hibernate.connection.useUnicode", true);
		properties.put("hibernate.connection.characterEncoding", "UTF-8");
		
		// c3p0
		properties.put("hibernate.c3p0.min_size", environment.getRequiredProperty("hibernate.c3p0.min_size"));
		properties.put("hibernate.c3p0.max_size", environment.getRequiredProperty("hibernate.c3p0.max_size"));
		properties.put("hibernate.c3p0.timeout", environment.getRequiredProperty("hibernate.c3p0.timeout"));
		return properties;
	}
	
	
}

