package com.datpt.streamingaudio.database.bo;

import java.io.Serializable;
import java.util.Date;

import lombok.Setter;

import lombok.Getter;

/**
 * @author DatPt
 * @date Feb 18, 2021
 * 
 */

@SuppressWarnings("serial")
@Getter
@Setter
public class AudioBO implements Serializable{

	private Long audioId;
	
	private String audioName;
	
	private String audioType;
	
	private String audioSrc;
	
	private String audioImageSrc;
	
	private String audioAuthor;
	
	private Long view;
	
	private String status;
	
	private Date createdDatetime;
	
	private String createdUser;
	
	private String updatedDatetime;
	
	private String updatedUser;
	
}
