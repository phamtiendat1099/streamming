package com.datpt.streamingaudio.database.bo;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * @author DatPt
 * @date Feb 17, 2021
 * 
 */

@SuppressWarnings("serial")
@Getter
@Setter
public class AudioCommentBO implements Serializable{
	
	private Long commentId;
	
	private Long audioId;
	
	private String conent;
	
	private String status;
	
	private Date createdDatetime;
	
	private String createdUser;
	
	private String updatedDatetime;
	
	private String updatedUser;
}
