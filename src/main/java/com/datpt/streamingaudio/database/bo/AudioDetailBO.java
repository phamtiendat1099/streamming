package com.datpt.streamingaudio.database.bo;

import java.io.Serializable;

import com.datpt.streamingaudio.database.entity.Audio;
import com.datpt.streamingaudio.database.entity.AudioComment;

import lombok.Getter;
import lombok.Setter;

/**
 * @author DatPt
 * @date Feb 18, 2021
 * 
 */

@SuppressWarnings("serial")
@Getter
@Setter
public class AudioDetailBO implements Serializable{
	
	private Audio audioInfo;
	
	private AudioComment commentInfo;

}
