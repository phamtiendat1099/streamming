package com.datpt.streamingaudio.database.bo;

import lombok.Getter;
import lombok.Setter;

/**
 * @author DatPt
 * @date Jan 22, 2021
 * 
 */

@Getter
@Setter
public class MusicBO {
	
	
	
	public MusicBO(Long id, String name, String src, String author, String thumb) {
		super();
		this.id = id;
		this.name = name;
		this.src = src;
		this.author = author;
		this.thumb = thumb;
	}

	private Long id;
	
	private String name;
	
	private String src;
	
	private String author;
	
	private String thumb;	
	
}
