package com.datpt.streamingaudio.common;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DataUtil {

	private static final String countryCode = "856";

	private static final Logger logger = LoggerFactory.getLogger(DataUtil.class);

	private DataUtil() {
	}

	public static String getExceptionDetail(Exception ex) {
		String error = "cannot get log";
		try {
			error = ExceptionUtils.getFullStackTrace(ex);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return error;
	}

	public static Long safeToLong(Object obj1, Long defaultValue) {
		Long result = defaultValue;
		if (obj1 != null) {
			if (obj1 instanceof BigDecimal) {
				return ((BigDecimal) obj1).longValue();
			}
			if (obj1 instanceof BigInteger) {
				return ((BigInteger) obj1).longValue();
			}
			try {
				result = Long.parseLong(obj1.toString());
			} catch (Exception ignored) {
			}
		}

		return result;
	}

	/**
	 * @param obj1 Object
	 * @return Long
	 */
	public static Long safeToLong(Object obj1) {
		return safeToLong(obj1, 0L);
	}

	@SuppressWarnings("rawtypes")
	public static boolean isNullObject(Object obj1) {
		if (obj1 == null) {
			return true;
		} else if (obj1 instanceof String) {
			return isNullOrEmpty((CharSequence) obj1.toString());
		} else if (obj1 instanceof Long && obj1.equals(0L)) {
			return true;
		} else {
			return obj1 instanceof List ? isNullOrEmpty((Collection) obj1) : false;
		}
	}

	public static boolean isNullOrEmpty(CharSequence cs) {
		int strLen;
		if (cs != null && (strLen = cs.length()) != 0) {
			for (int i = 0; i < strLen; ++i) {
				if (!Character.isWhitespace(cs.charAt(i))) {
					return false;
				}
			}

			return true;
		} else {
			return true;
		}
	}

	public static String validPath(String strPath) {
		if (!strPath.endsWith("/")) {
			strPath += "/";
		}
		return strPath;
	}

	public static boolean isNullOrEmpty(Collection<?> collection) {
		return collection == null || collection.isEmpty();
	}

	public static String standardISDN(String msisdn) {
		try {
			if (msisdn.startsWith(countryCode)) {
				return msisdn.substring(countryCode.length());
			} else if (msisdn.startsWith("0")) {
				return msisdn.substring(1);
			}
			return msisdn;
		} catch (Exception ex) {
			return null;
		}
	}

	public static String formatMSISDN(String isdn) {
		try {
			if (!isdn.startsWith(countryCode)) {
				return countryCode + isdn;
			}
			return isdn;
		} catch (Exception ex) {
			return null;
		}
	}

	/**
	 * @param obj1 Object
	 * @return String
	 */
	public static String safeToString(Object obj1) {
		return safeToString(obj1, "");
	}

	/**
	 * @param obj1 Object
	 * @return String
	 */
	public static String safeToString(Object obj1, String defaultValue) {
		if (obj1 == null) {
			return defaultValue;
		}

		return obj1.toString();
	}

}
