package com.datpt.streamingaudio.common;

public class Constant {


	public static final class STATUS {
		public static final String ACTIVE = "1";
		public static final String IN_ACTIVE = "0";
		public static final String WAITING_APPROVE = "2";
		public static final String REJECT = "3";
	}
	
	public static final class OTP_TYPE{
		public static final String SIGN_FILE = "SIGN_FILE";
	}
	
	public static final class FILE_TYPE{
		public static final String IMAGE = "IMAGE";
		public static final String AUDIO = "AUDIO";
	}
}
