package com.datpt.streamingaudio.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class CommonService {
	
	protected static final Logger logger = LoggerFactory.getLogger(CommonService.class);
	
	@Autowired
	protected DictionaryHelper dicHelper;
	
	public void processException(Exception ex, String language) throws Exception {
		
		throw new LogicException(dicHelper.getMessage(ex.getMessage(), language));
		
	}
	
}
