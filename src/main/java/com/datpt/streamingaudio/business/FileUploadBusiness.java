package com.datpt.streamingaudio.business;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author DatPt
 * @date Feb 18, 2021
 * 
 */

public interface FileUploadBusiness {

	public String uploadFile(MultipartFile file) throws Exception;
	
}
