package com.datpt.streamingaudio.business.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datpt.streamingaudio.business.AudioBusiness;
import com.datpt.streamingaudio.common.Constant;
import com.datpt.streamingaudio.common.Crypt;
import com.datpt.streamingaudio.common.DataUtil;
import com.datpt.streamingaudio.common.DateTimeUtils;
import com.datpt.streamingaudio.database.bo.AudioDetailBO;
import com.datpt.streamingaudio.database.entity.Audio;
import com.datpt.streamingaudio.database.entity.AudioComment;
import com.datpt.streamingaudio.database.entity.User;
import com.datpt.streamingaudio.database.repo.AudioCommentRepo;
import com.datpt.streamingaudio.database.repo.AudioRepo;
import com.datpt.streamingaudio.database.repo.UserRepo;

/**
 * @author DatPt
 * @date Apr 1, 2021
 * 
 */
@Service
public class AudioBusinessImpl implements AudioBusiness{

	@Autowired
	private UserRepo userRepo;
	
	@Autowired
	private AudioRepo audioRepo;
	
	@Autowired
	private AudioCommentRepo audioCommentRepo;
	
	@Override
	public String addNewAudio(String username, String password, String audioName, String audioType, String audioSrc,
			String audioImageSrc, String audioAuthor) throws Exception {
		
		username = username.toUpperCase();
		
		User user = userRepo.findOneByUsernameAndPassword(username, Crypt.encrypt(password, "datpt"));
		
		if(DataUtil.isNullObject(user)) {
			throw new Exception("Tài khoản hoặc mật khẩu chưa chính xác !");
		}
		
		if (!"ADMIN".equals(user.getRole())) {
			throw new Exception("Bạn không có quyền !");
		}
		
		Audio audio = new Audio();
		audio.setAudioName(audioName);
		audio.setAudioType(audioType);
		audio.setAudioSrc(audioSrc);
		audio.setAudioSrc(audioSrc);
		audio.setAudioAuthor(audioAuthor);
		audio.setCreatedUser(username);
		audio.setCreatedDatetime(DateTimeUtils.getSysDate());
		audio.setStatus(Constant.STATUS.ACTIVE);
		
		audioRepo.save(audio);
		
		return "Thêm bài hát mới thành !";
	}

	@Override
	public String upadteAudio(String username, String password, Long audioId, String audioName, String audioType,
			String audioImageSrc, String audioAuthor) throws Exception {
		username = username.toUpperCase();	
		
		User user = userRepo.findOneByUsernameAndPassword(username, Crypt.encrypt(password, "datpt"));
		
		if(DataUtil.isNullObject(user)) {
			throw new Exception("Tài khoản hoặc mật khẩu chưa chính xác !");
		}
		
		if (!"ADMIN".equals(user.getRole())) {
			throw new Exception("Bạn không có quyền !");
		}
		
		Audio audio = audioRepo.findOneByAudioId(audioId);
		if(DataUtil.isNullObject(audio)) {
			throw new Exception("Không tìm thấy bài hát !");
		}
		
		audio.setAudioName(audioName);
		audio.setAudioType(audioType);
		audio.setAudioAuthor(audioAuthor);
		audio.setUpdatedUser(username);
		audio.setUpdatedDatetime(DateTimeUtils.getSysDate());
		audio.setStatus(Constant.STATUS.ACTIVE);
		
		audioRepo.save(audio);
		
		return "Cập  bài hát mới thành !";
	}

	@Override
	public String deleteAudio(String username, String password, Long audioId) throws Exception {
		
		username = username.toUpperCase();
		
		User user = userRepo.findOneByUsernameAndPassword(username, Crypt.encrypt(password, "datpt"));
		
		if(DataUtil.isNullObject(user)) {
			throw new Exception("Tài khoản hoặc mật khẩu chưa chính xác !");
		}
		
		if (!"ADMIN".equals(user.getRole())) {
			throw new Exception("Bạn không có quyền !");
		}
		
		Audio audio = audioRepo.findOneByAudioId(audioId);
		if(DataUtil.isNullObject(audio)) {
			throw new Exception("Không tìm thấy bài hát !");
		}
		
		audio.setStatus(Constant.STATUS.IN_ACTIVE);
		audio.setUpdatedUser(username);
		audio.setUpdatedDatetime(DateTimeUtils.getSysDate());
		
		return "Deteled !";
	}

	@Override
	public AudioDetailBO getDetailAudio(Long audioId) throws Exception {
		
		AudioDetailBO auBo = new AudioDetailBO();
		
		Audio audio = audioRepo.findOneByAudioId(audioId);
		if(DataUtil.isNullObject(audio)) {
			throw new Exception("Không tìm thấy bài hát !");
		}
		
		auBo.setAudioInfo(audio);
		
		AudioComment auComment = audioCommentRepo.findByAudioId(audioId);
		
		auBo.setCommentInfo(auComment);
		
		return auBo;
	}

	@Override
	public List<Audio> getListAudio() {
	
		List<Audio> lstAudio = audioRepo.findByStatus(Constant.STATUS.ACTIVE);
		
		return lstAudio;
	}

	@Override
	public String upView(Long audioId) throws Exception {
		Audio audio = audioRepo.findOneByAudioId(audioId);
		if(DataUtil.isNullObject(audio)) {
			throw new Exception("Không tìm thấy bài hát !");
		}
		
		audio.setView(audio.getView()+1);
		
		audioRepo.save(audio);
		
		return "View update!";
	}

	@Override
	public String comment(String username, String password, Long audioId, String content) throws Exception {
		
		username = username.toUpperCase();
		
		User user = userRepo.findOneByUsernameAndPassword(username, Crypt.encrypt(password, "datpt"));
		
		if(DataUtil.isNullObject(user)) {
			throw new Exception("Tài khoản hoặc mật khẩu chưa chính xác !");
		}
		
		Audio audio = audioRepo.findOneByAudioId(audioId);
		if(DataUtil.isNullObject(audio)) {
			throw new Exception("Không tìm thấy bài hát !");
		}
		
		AudioComment aucomment = new AudioComment();
		aucomment.setAudioId(audioId);
		aucomment.setContent(content);
		aucomment.setUpdatedUser(username);
		aucomment.setUpdatedDatetime(DateTimeUtils.getSysDate());
		
		audioCommentRepo.save(aucomment);
		
		return "Bình luận thành công !";
		
	}

}
