package com.datpt.streamingaudio.business.impl;

import java.io.File;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.datpt.streamingaudio.business.FileUploadBusiness;
import com.datpt.streamingaudio.business.UserBusiness;
import com.datpt.streamingaudio.common.Constant;
import com.datpt.streamingaudio.common.DateTimeUtils;
import com.datpt.streamingaudio.database.entity.FileUpload;
import com.datpt.streamingaudio.database.repo.FileUploadRepo;
import com.google.common.io.Files;

/**
 * @author DatPt
 * @date Feb 18, 2021
 * 
 */

@Service
public class FileUploadBusinessImpl implements FileUploadBusiness{
	
	@Autowired
	private FileUploadRepo fileUploadRepo;

	@Override
	public String uploadFile(MultipartFile multipartFile)
			throws Exception {
		
		FileUpload fileUpload = new FileUpload();
		
		
		String onlyFileName = Files.getNameWithoutExtension(multipartFile.getOriginalFilename());
		
		String extension = Files.getFileExtension(multipartFile.getOriginalFilename());
		
		Date dateNow = DateTimeUtils.getSysDate();
		
		String fileName = onlyFileName+"_"+dateNow.getTime()+"."+extension;
		
		File file = new File(this.getFolderUpload(fileUpload.getFileType()), fileName);
		multipartFile.transferTo(file);
		
		
		fileUpload.setStatus(Constant.STATUS.IN_ACTIVE);
		fileUpload.setCreatedDatetime(dateNow);
		fileUpload.setFileSrc(file.getPath());
		
		fileUpload = fileUploadRepo.save(fileUpload);
		
		return fileUpload.getFileSrc();
	}
	
	public File getFolderUpload(String folder) {
	    File folderUpload = new File(System.getProperty("user.home") + "/streamming_audio/"+folder);
	    if (!folderUpload.exists()) {
	      folderUpload.mkdirs();
	    }
	    return folderUpload;
	}

}
