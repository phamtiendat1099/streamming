package com.datpt.streamingaudio.business.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datpt.streamingaudio.business.UserBusiness;
import com.datpt.streamingaudio.common.Constant;
import com.datpt.streamingaudio.common.Crypt;
import com.datpt.streamingaudio.common.DataUtil;
import com.datpt.streamingaudio.common.DateTimeUtils;
import com.datpt.streamingaudio.database.entity.User;
import com.datpt.streamingaudio.database.repo.UserRepo;

/**
 * @author DatPt
 * @date Feb 17, 2021
 * 
 */

@Service
public class UserBusinessImpl implements UserBusiness{
	
	@Autowired
	private UserRepo userRepo;

	@Override
	public String login(String username, String password) throws Exception {
		
		username = username.toUpperCase();
		
		User user = userRepo.findOneByUsernameAndPassword(username, Crypt.encrypt(password, "datpt"));
		
		if(DataUtil.isNullObject(user)) {
			throw new Exception("Tài khoản hoặc mật khẩu chưa chính xác !");
		}
		
		return "Đăng nhập thành công !";
	}

	@Override
	public String createUser(String email, String username, String password) throws Exception {
		
		username = username.toUpperCase();
		
		User user = userRepo.findOneByEmail(email);
		
		if(!DataUtil.isNullObject(user)) {
			throw new Exception("Email đã tồn tại !");
		}
		
		user = userRepo.findOneByEmail(username);
		
		if (!DataUtil.isNullObject(user)) {
			throw new Exception("Username đã tồn tại !");
		}
		
		user = new User();

		user.setEmail(email);
		user.setUsername(username);
		user.setPassword(Crypt.encrypt(password, "datpt"));
		user.setStatus(Constant.STATUS.ACTIVE);
		user.setCreatedDatetime(DateTimeUtils.getSysDate());
		user.setCreatedUser("SYSTEM");
		
		userRepo.save(user);
		
		return "Tạo tài khoản mới thành công !";
	}

	@Override
	public String changePassword(String username, String oldPassword, String newPassword) throws Exception {
		
		username = username.toUpperCase();
		
		User user = userRepo.findOneByUsernameAndPassword(username, Crypt.encrypt(oldPassword, "datpt"));
		
		if(DataUtil.isNullObject(user)) {
			throw new Exception("Tài khoản hoặc mật khẩu chưa chính xác !");
		}
		
		user.setPassword(Crypt.encrypt(newPassword, "datpt"));
		user.setUpdatedDatetime(DateTimeUtils.getSysDate());
		user.setUpdatedUser(username);
		
		userRepo.save(user);
		
		return "Đổi mật khẩu thành công !";
	}
	
}
