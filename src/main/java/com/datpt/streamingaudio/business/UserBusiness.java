package com.datpt.streamingaudio.business;


/**
 * @author DatPt
 * @date Feb 17, 2021
 * 
 */

public interface UserBusiness {
	
	public String createUser(
		String email,
		String username,
		String password
	) throws Exception;
	
	
	public String login(String username, String password) throws Exception;
	
	public String changePassword(String username, String oldPassword, String newPassword) throws Exception;
}
