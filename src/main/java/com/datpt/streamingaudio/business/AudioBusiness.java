package com.datpt.streamingaudio.business;

import java.util.List;

import com.datpt.streamingaudio.database.bo.AudioDetailBO;
import com.datpt.streamingaudio.database.entity.Audio;

/**
 * @author DatPt
 * @date Feb 18, 2021
 * 
 */

public interface AudioBusiness {

	public String addNewAudio(
			String username,
			String password,
			String audioName,
			String audioType,
			String audioSrc,
			String audioImageSrc,
			String audioAuthor
			) throws Exception;
	
	public String upadteAudio(
			String username,
			String password,
			Long audioId,
			String audioName,
			String audioType,
			String audioImageSrc,
			String audioAuthor
			) throws Exception;
	
	public String deleteAudio(
			String username,
			String password,
			Long audioId
			) throws Exception;
	
	public AudioDetailBO getDetailAudio(
			Long audioId) throws Exception;
	
	public List<Audio> getListAudio() throws Exception;
	
	public String upView(
			Long audioId) throws Exception;
	
	public String comment(
			String username,
			String password,
			Long audioId,
			String content) throws Exception;
	
}
