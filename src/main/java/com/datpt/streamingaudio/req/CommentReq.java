package com.datpt.streamingaudio.req;

import lombok.Getter;
import lombok.Setter;

/**
 * @author DatPt
 * @date Apr 1, 2021
 * 
 */

@Getter
@Setter
public class CommentReq {
	
	private String username;
	
	private String password;
	
	private Long audioId;
	
	private String content;

}
