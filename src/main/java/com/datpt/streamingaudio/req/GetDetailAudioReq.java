package com.datpt.streamingaudio.req;

import lombok.Getter;
import lombok.Setter;

/**
 * @author DatPt
 * @date Apr 1, 2021
 * 
 */

@Getter
@Setter
public class GetDetailAudioReq {
	
	private Long audioId;

}
