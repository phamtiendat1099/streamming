package com.datpt.streamingaudio.req;

import lombok.Getter;
import lombok.Setter;

/**
 * @author DatPt
 * @date Feb 17, 2021
 * 
 */

@Getter
@Setter
public class LoginReq {

	private String username;
	
	private String password;
	
}
