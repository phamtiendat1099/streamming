package com.datpt.streamingaudio.req;

import lombok.Getter;
import lombok.Setter;

/**
 * @author DatPt
 * @date Feb 17, 2021
 * 
 */

@Getter
@Setter
public class CreateUserReq {

	private String email;
	
	private String username;
	
	private String password;
	
}
