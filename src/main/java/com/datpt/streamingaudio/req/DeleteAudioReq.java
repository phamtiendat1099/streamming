package com.datpt.streamingaudio.req;

import lombok.Setter;

import lombok.Getter;

/**
 * @author DatPt
 * @date Apr 1, 2021
 * 
 */

@Getter
@Setter
public class DeleteAudioReq {
	
	private String username;
	
	private String password;
	
	private Long audioId;

}
