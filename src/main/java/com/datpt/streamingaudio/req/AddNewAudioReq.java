package com.datpt.streamingaudio.req;

import lombok.Getter;
import lombok.Setter;

/**
 * @author DatPt
 * @date Apr 1, 2021
 * 
 */

@Getter
@Setter
public class AddNewAudioReq {
	
	private String username;
	
	private String password;
	
	private String audioName;
	
	private String audioType;
	
	private String audioSrc;
	
	private String audioImageSrc;
	
	private String audioAuthor;

}
