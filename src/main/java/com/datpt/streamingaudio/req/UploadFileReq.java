package com.datpt.streamingaudio.req;

import org.springframework.web.multipart.MultipartFile;

import lombok.Getter;
import lombok.Setter;

/**
 * @author DatPt
 * @date Feb 18, 2021
 * 
 */

@Getter
@Setter
public class UploadFileReq {

	private String username;
	
	private String password;
	
	private String fileType;
	
	private MultipartFile multipartFile;
	
}
