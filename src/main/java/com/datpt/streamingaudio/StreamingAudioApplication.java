package com.datpt.streamingaudio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StreamingAudioApplication {

	public static void main(String[] args) {
		SpringApplication.run(StreamingAudioApplication.class, args);
	}

}
